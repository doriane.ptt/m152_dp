<?php
/*
 *  Author  :   Doriane Pott
 *  Class   :   P3A
 *  Date    :   2020/01/20
 *  Desc.   :   publish post page
*/

?>
<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
        <link href="assets/css/bootstrap.css" rel="stylesheet">
        <!--[if lt IE 9]>
          <script src="//html5shim.googlecode.com/svn/trunk/html5.js"></script>
        <![endif]-->
        <link href="assets/css/facebook.css" rel="stylesheet">
    <title>M152 - Infobook - Post</title>
</head>
<body>
    <div class="well"> 
		<form class="form-horizontal" role="form" action="upload.php" enctype="multipart/form-data">
		    <h4>What's New</h4>
		    <div class="form-group" style="padding:14px;">
		        <textarea class="form-control" placeholder="Write something..."></textarea>
		    </div>
            <button id="action" class="btn btn-light pull-right" type="submit" value="boost">Boost Post</button>
            <button id="action" class="btn btn-primary pull-right" type="submit" value="send">Publish</button>
            <ul class="list-inline">
                <li><a href=""><i class="glyphicon glyphicon-upload"></i></a></li>
                <li><i class="glyphicon glyphicon-camera"></i><input type="file" name="img"></li>
                <li><a href=""><i class="glyphicon glyphicon-map-marker"></i></a></li>
            </ul>
		</form>
	</div>
</body>
</html>