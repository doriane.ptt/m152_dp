<?php
/*
 *  Author  :   Doriane Pott
 *  Class   :   P3A
 *  Date    :   2020/01/20
 *  Desc.   :   check page of the uploaded file
*/

// Init
var_dump($_FILES);

$directory = 'upload/';

// file data
$file_extension = strrchr($_FILES['img']['name'], '.');
$file_name = basename($_FILES['img']['name']);
$file_size = filesize($_FILES['img']['tmp_name']);

// list all the authorised extension
$extensions = array('.png', '.gif', '.jpg', '.jpeg');
$MAX_FILE_SIZE = 3145728;    // 3MB in bytes
$MAX_POST_SIZE = 73400320;  // 70MB in bytes

// Process

// verify if the extension is one of the authorised.
if (!in_array($file_extension, $extensions)) {
    $error = 'Image file only (.png, .gif, .jpg, .jpeg)';
}
// verify the size
if ($file_size > $MAX_FILE_SIZE) {
    $error = 'File too heavy.';
}

if (!isset($error)) {
    $file_name = strtr($file_name,
    'ÀÁÂÃÄÅÇÈÉÊËÌÍÎÏÒÓÔÕÖÙÚÛÜÝàáâãäåçèéêëìíîïðòóôõöùúûüýÿ', 
    'AAAAAACEEEEIIIIOOOOOUUUUYaaaaaaceeeeiiiioooooouuuuyy');
    
    $fichier = preg_replace('/([^.a-z0-9]+)/i', '-', $fichier);

    if (isset($_FILES['img'])) {
        $inc = 1;
        // process to avoid file with same name
        while (file_exists($directory . $file_name)) {
            $inc++;
            $file_name .= $inc;
        }

        if (move_uploaded_file($_FILES['img']['tmp_name'], $directory . $file_name)) {
            echo "Upload effectué avec succèes!";
        } else {
            echo 'Echec de l\'upload!';
            echo str($_FILES['img']['error']);
            error_log($_FILES['img']['error']);
        }
    }

}
else {
    echo $error;
}